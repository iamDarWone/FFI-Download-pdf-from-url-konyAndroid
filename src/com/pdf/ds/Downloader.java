package com.pdf.ds;


import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.konylabs.android.KonyMain;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Downloader
{
  public  static final int MEGABYTE = 1048576;
  static final String NOMBRE_DIRECTORIO = "Compensar";
  static String NOMBRE_DOCUMENTO = "appSalud.pdf";
  private static Activity actividad;

  
  public static void DescargarPDF(String fileURL, File directory,String name)
  {
    try
    {
    	//donwload pdf 
      URL url = new URL(fileURL);
      HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
      
      urlConnection.connect();
      
      InputStream inputStream = urlConnection.getInputStream();
      FileOutputStream fileOutputStream = new FileOutputStream(directory);
      //int totalSize = urlConnection.getContentLength();
      
      byte[] buffer = new byte[1048576];
      int bufferLength = 0;
      while ((bufferLength = inputStream.read(buffer)) > 0) {
        fileOutputStream.write(buffer, 0, bufferLength);
      }
      fileOutputStream.close();
    }
    catch (FileNotFoundException e)
    {
      e.printStackTrace();
      Log.e("pdf error ", e.toString());
    }
    catch (MalformedURLException e)
    {
      e.printStackTrace();
      Log.e("pdf error ", e.toString());
    }
    catch (IOException e)
    {
      e.printStackTrace();
      Log.e("pdf error ", e.toString());
    }
  }
  
  public static void llamarDescarga(String URL)
  {
	  Context context = KonyMain.getActivityContext();
	  actividad = (Activity)context;	
    File file = null;
	try {
		file = crearFichero(NOMBRE_DOCUMENTO);
	} catch (IOException e2) {
		e2.printStackTrace();
	}
    try
    {
      file.createNewFile();
    }
    catch (IOException e1)
    {
      e1.printStackTrace();
    }
    DescargarPDF(URL, file,NOMBRE_DOCUMENTO);
    
  
    Log.v("pdf load ", file.getAbsolutePath());
    
   
    	viewPdf(file );
    	Toast.makeText(actividad.getApplicationContext(), "Generado! "+file.getAbsolutePath(), Toast.LENGTH_LONG).show();
		
		//boolean generoPdf = true;
    
  
  }
public static File crearFichero(String nombreFichero) throws IOException  {
	File ruta = getRuta();
	File fichero = null;
	if (ruta != null){
		fichero = new File(ruta, nombreFichero);
	}
	/**else{
		fichero = new File(actividad.getFilesDir(), NOMBRE_DOCUMENTO);
	}*/
	return fichero;
}

public static File getRuta() {
	// El fichero sera almacenado en un directorio dentro del directorio
		// Descargas
		File ruta = null;
		if (Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState())) {
			ruta = new File(
					Environment
							.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
					NOMBRE_DIRECTORIO);
			Toast.makeText(actividad.getApplicationContext(), "creado!, "+ruta.getAbsolutePath(), Toast.LENGTH_LONG).show();
			   
			
			if (ruta != null) {
				if (!ruta.mkdirs()) {
					if (!ruta.exists()) {
						Toast.makeText(actividad.getApplicationContext(), "banderas null!, "+ruta.exists(), Toast.LENGTH_LONG).show();
						
						return null;
					}
				}
			}
		}
		return ruta;
}
  

public static void viewPdf(File file) {
    Context context = KonyMain.getActivityContext();
    //File file = new File("/storage/emulated/0/Download/Compensar/appSalud.pdf");

    Intent intent = new Intent(Intent.ACTION_VIEW);

    intent.setDataAndType(Uri.fromFile(file),"application/pdf");

    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

       try {
        context.startActivity(intent);
       } catch (ActivityNotFoundException e) {
           // No application to view, ask to download one
                           Intent marketIntent = new Intent(Intent.ACTION_VIEW);
                           marketIntent.setData(Uri.parse("market://details?id=com.adobe.reader"));
                           context.startActivity(marketIntent);
                       }
   }
}